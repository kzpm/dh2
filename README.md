## DH2 Diffie Hellmann testscript



### Script

DH2 is een script waarmee je aan kunt tonen, hoe de DH keyexchange werkt.
Als input zijn de getallen voor de private keys van Alice en Bob nodig.

Het script rekent vervolgens de versleuteling uit en geeft dit weer in getal.
Tot slot worden het bericht van Alice en Bob ontsleuteld. Deze getallen komen exact overeen.


